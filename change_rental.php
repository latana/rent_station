<html>
    <head>
        <title>Change the rental</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body
            {
                background: center 100% / 100% 100% url("./images/vhs.jpg");
                font-family: 'Courier New', Courier, monospace;
                color: white;
            }
            div.desc
            {
                text-align: center;
                color: white;
                font-family: 'Courier New', Courier, monospace;
                font-size: 50px;
            }
            table
            {
                width: 100%;
                font-family: 'Courier New', Courier, monospace;
                color: white;
                border-color: white;
            }
            div.footer
            {
                position: absolute;
                right: 0;
                bottom: 0;
            }
        </style>
    </head>

    <body>
        <?php
        include('config.php');	
        $link = mysqli_connect($host, $user, $password, $database)
            or die('Error: Unable to connect: ' . mysqli_connect_error());
            
        if($_POST['id'])
        {
            $id = $_POST['id'];
            $SQLrental = "SELECT Clients.ID_client, Clients.surname, Clients.name, Clients.middle_name, Movies.ID_movie, Movies.title, Rental.rental_date, Rental.return_date, Rental.status FROM Clients INNER JOIN Rental ON Rental.client=Clients.ID_client INNER JOIN Movies ON Rental.movie=Movies.ID_movie WHERE Rental.ID_rental=$id";
            $rental = mysqli_query($link,$SQLrental);
            while ($res = mysqli_fetch_array($rental, MYSQLI_NUM))
            {
                echo "<form action='change_rental_form_action.php?id=$id' method='POST'>
                    ФИО:
                    <select name='fio'>
                        <option value='$res[0]'>$res[1] $res[2] $res[3]</option>";
                        $SQLfio = 'SELECT ID_client, surname, name, middle_name FROM Clients ORDER BY surname';
                        $fio = mysqli_query($link,$SQLfio);
                        while ($result = mysqli_fetch_array($fio, MYSQLI_NUM))
                        {
                            echo "<option value='$result[0]'>$result[1] $result[2] $result[3]</option>";
                        }
                    echo "</select>
                    <input formaction='add_client.php' type='submit' value='New client'>
                    <br><br>
                    Фильм:
                    <select name='movie'>
                        <option value='$res[4]'> $res[5] </option>";
                            $SQLmovies = 'SELECT ID_movie, title FROM Movies ORDER BY title';
                            $movies = mysqli_query($link,$SQLmovies);
                            while ($result = mysqli_fetch_array($movies, MYSQLI_NUM))
                            {
                                echo "<option value='$result[0]'> $result[1] </option>";
                            }
                    echo "</select>
                    <input formaction='add_movie.php' type='submit' value='New movie'>
                    <br><br>
                    Дата проката: <input type='date' value='$res[6]' name='rental_date'>
                    <br><br>
                    Дата возврата: <input type='date' value='$res[7]' name='return_date'>
                    <br><br>
                    Статус:
                    <select name='status'>
                        <option> $res[8] </option>
                        <option> not lost </option>
                        <option> lost </option>
                        <option> pending </option>
                    </select>
                    <p><input type='submit' value='Send'></p>
                </form>";
            }
        }
        else
        {
            echo "<div class='desc'> Вернитесь назад и выберите то, что хотите изменить </div>";
        }
        ?>
        
        <div class="footer"><a href="current_state.php"> <img src="./images/back.png"> </a></div>
    </body>
</html>