<html>
    <head>
        <title>Movies</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body
            {
                background: center 100% / 100% 100% url("./images/vhs.jpg");
            }
            div.gallery
            {
                margin-bottom: 30px;
                margin-right: 15px;
                border: 5px ridge white;
                float: left;
                height: 300px;
            }
            div.gallery:hover
            {
                border: 5px groove red;
            }
            div.gallery img
            {
                height: 100%;
                width: auto;
            }
            div.desc
            {
                padding: 10px;
                text-align: center;
                color: white;
                font-family: 'Courier New', Courier, monospace;
            }
            div.footer
            {
                position: absolute;
                right: 0;
                bottom: 0;
            }
        </style>
    </head>

    <body>
    <?php
        include('config.php');	
        $link = mysqli_connect($host, $user, $password, $database)
            or die('Error: Unable to connect: ' . mysqli_connect_error());
            
        $SQLquery = 'SELECT ID_movie, title, poster FROM Movies ORDER BY title';
        $SQLresult = mysqli_query($link,$SQLquery);
    
        while ($result = mysqli_fetch_array($SQLresult, MYSQLI_NUM))
        {
            echo "<div class = 'gallery'>
                <a target='_self' href='movie.php?id=".$result[0]."'>
                    <img src = '".$result[2]."'>
                    <div class = 'desc'>" .$result[1]. "</div>
                </a>
            </div>";
        }
    ?>
        <div class="gallery">
            <a target = "_self" href = "add_movie.php">
                <img src = "./images/add.png">
            </a>
            <div class = "desc"> Add movie </div>
        </div>
        <div class="gallery">
            <a target="_self" href="remove_movie.php">
                <img src="./images/remove.png">
            </a>
            <div class="desc">Remove movie</div>
        </div>
    <?php
        mysqli_free_result($SQLresult);
        mysqli_close($link);
    ?>
        <div class="footer"><a href="index.html"> <img src="./images/back.png"> </a></div>
    </body>
</html>