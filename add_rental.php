<html>
    <head>
        <title>Add rental</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body
            {
                background: center 100% / 100% 100% url("./images/vhs.jpg");
                font-family: 'Courier New', Courier, monospace;
                color: white;
            }
            table
            {
                width: 100%;
                font-family: 'Courier New', Courier, monospace;
                color: white;
                border-color: white;
            }
            div.footer
            {
                position: absolute;
                right: 0;
                bottom: 0;
            }
        </style>
    </head>

    <body>
        <?php
        include('config.php');	
        $link = mysqli_connect($host, $user, $password, $database)
            or die('Error: Unable to connect: ' . mysqli_connect_error());
        
        echo "<form action='add_rental_form_action.php' method='POST'>
            ФИО:
            <select name='fio'>
                <option>ФИО</option>";
                $SQLfio = 'SELECT ID_client, surname, name, middle_name FROM Clients ORDER BY surname';
                $fio = mysqli_query($link,$SQLfio);
                while ($result = mysqli_fetch_array($fio, MYSQLI_NUM))
                {
                    echo "<option value='$result[0]'> $result[1] $result[2] $result[3] </option>";
                }
            echo "</select>
            <input formaction='add_client.php' type='submit' value='New client'>
            <br><br>
            Фильм:
            <select name='movie'>
                <option>Фильм</option>";
                $SQLmovies = "SELECT id_movie, title FROM Movies ORDER BY title";
                $movies = mysqli_query($link,$SQLmovies);
                while ($result = mysqli_fetch_array($movies, MYSQLI_NUM))
                {
                    echo "<option value='$result[0]'> $result[1] </option>";
                }
            echo "</select>
            <input formaction='add_movie.php' type='submit' value='New movie'>
            <br><br>
            Дата проката: <input type='date' name='rental_date'>
            <br><br>
            Дата возврата: <input type='date' name='return_date'>
            <br><br>
            Статус:
            <select name='status'>
                <option>Статус</option>
                <option>not lost</option>
                <option>lost</option>
                <option>pending</option>
            </select>
            <p><input type='submit' value='Send'></p>
        </form>";
        ?>
        
        <div class="footer"><a href="current_state.php"> <img src="./images/back.png"> </a></div>
    </body>
</html>