<html>
    <head>
        <title>Add client</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body
            {
                background: center 100% / 100% 100% url("./images/vhs.jpg");
                color: white;
                font-family: 'Courier New', Courier, monospace;
            }
            div.desc
            {
                text-align: center;
                color: white;
                font-family: 'Courier New', Courier, monospace;
                font-size: 50px;
            }
            div.footer
            {
                position: absolute;
                right: 0;
                bottom: 0;
            }
        </style>
    </head>

    <body>
        <?php
        include('config.php');	
        $link = mysqli_connect($host, $user, $password, $database)
            or die('Error: Unable to connect: ' . mysqli_connect_error());

        $surname = mysqli_real_escape_string($link, $_POST['surname']);
        $name = mysqli_real_escape_string($link, $_POST['name']);
        $middle_name = mysqli_real_escape_string($link, $_POST['middle_name']);
        $passport_series = mysqli_real_escape_string($link, $_POST['passport_series']);
        $passport_number = mysqli_real_escape_string($link, $_POST['passport_number']);
        $address = mysqli_real_escape_string($link, $_POST['address']);
        $discount = mysqli_real_escape_string($link, $_POST['discount']);
        
        $SQLquery = "INSERT INTO Clients VALUES (NULL, '$surname', '$name', '$middle_name', '$passport_series', '$passport_number', '$address', $discount)";
        if(mysqli_query($link, $SQLquery))
        {
            echo "<div class='desc'> Клиент успешно добавлен </div>";
        }
        else
        {
            echo "<div class='desc'> Произошла ошибка </div>";
        }

        mysqli_close($link);
        ?>
        <div class="footer"><a href="clients.php"> <img src="./images/back.png"> </a></div>
    </body>
</html>