<html>
    <head>
        <title>Current state</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body
            {
                background: center 100% / 100% 100% url("./images/vhs.jpg");
            }
            table
            {
                width: 100%;
                font-family: 'Courier New', Courier, monospace;
                color: white;
                border-color: white;
            }
            div.footer
            {
                position: absolute;
                right: 0;
                bottom: 0;
            }
        </style>
    </head>

    <body>
        <?php
        include('config.php');	
        $link = mysqli_connect($host, $user, $password, $database)
            or die('Error: Unable to connect: ' . mysqli_connect_error());
            
        $SQLquery = "SELECT Rental.ID_rental, Clients.surname, Clients.name, Clients.middle_name, Movies.title, Rental.rental_date, Rental.return_date, Rental.status FROM Rental INNER JOIN Clients ON Rental.client=Clients.ID_client INNER JOIN Movies ON Rental.movie=Movies.ID_movie ORDER BY Rental.rental_date";
        $SQLresult = mysqli_query($link,$SQLquery);
        ?>

        <form method='POST'>
            <table border='2'>
                <th>Выбрать</th>
                <th>ФИО</th>
                <th>Фильм</th>
                <th>Дата проката</th>
                <th>Дата возврата</th>
                <th>Статус</th>
                <?php
                while ($result = mysqli_fetch_array($SQLresult, MYSQLI_NUM))
                {
                    echo "
                    <tr>
                        <td><input type='radio' name='id' value='".$result[0]."'></td>
                        <td> $result[1] $result[2] $result[3] </td>
                        <td> $result[4] </td>
                        <td> $result[5] </td>
                        <td> $result[6] </td>
                        <td> $result[7] </td>
                    </tr>";
                }
                ?>
            </table>
            <br>
            <input type="submit" formaction='change_rental.php' value="Изменить">
            <input type="submit" formaction='remove_rental_form_action.php' value="Удалить">
        </form>
        
        <p><a href="add_rental.php">
            <button>Добавить</button>
        </a></p>
        
        <div class="footer"><a href="index.html"> <img src="./images/back.png"> </a></div>
    </body>
</html>