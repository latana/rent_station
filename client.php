<html>
    <head>
        <title>About client</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body
            {
                background: center 100% / 100% 100% url("./images/vhs.jpg");
                font-family: 'Courier New', Courier, monospace;
                color: white;
            }
            table
            {
                font-family: 'Courier New', Courier, monospace;
                color: white;
                border-color: white;
            }
            div.footer
            {
                position: absolute;
                right: 0;
                bottom: 0;
            }
        </style>
    </head>

    <body>
        <?php $id = $_GET['id'];

        include('config.php');	
        $link = mysqli_connect($host, $user, $password, $database)
            or die('Error: Unable to connect: ' . mysqli_connect_error());

        $SQLclient = "SELECT Clients.surname, Clients.name, Clients.middle_name, Clients.passport_series, Clients.passport_number, Clients.address, Discount.category, Discount.discount FROM Clients INNER JOIN Discount ON Discount.ID_discount=Clients.discount WHERE Clients.ID_client=$id";
        $client = mysqli_query($link,$SQLclient);

        $SQLhistory = "SELECT Rental.ID_rental, Movies.title, Rental.rental_date, Rental.return_date, Movies.rental_cost, Movies.deposit_amount, Rental.status FROM Clients INNER JOIN Discount ON Clients.discount=Discount.ID_discount INNER JOIN Rental ON Rental.client=Clients.ID_client INNER JOIN Movies ON Rental.movie=Movies.ID_movie WHERE Clients.ID_client=$id";
        $history = mysqli_query($link,$SQLhistory);

        while ($result = mysqli_fetch_array($client, MYSQLI_NUM))
        {
            echo "
            <strong>Фамилия:</strong> $result[0] <br>
            <strong>Имя:</strong> $result[1] <br>
            <strong>Отчество:</strong> $result[2] <br>
            <strong>Паспортные данные:</strong> $result[3] $result[4] <br>
            <strong>Адрес:</strong> $result[5] <br>
            <strong>Категория:</strong> $result[6] <br>
            <strong>Размер скидки:</strong> $result[7]%";
        }
        
        $s=0;

        echo "<br><br>
        <table border='2'>
            <th>Фильм</th>
            <th>Дата проката</th>
            <th>Дата возврата</th>
            <th>Аренда (руб/сут)</th>
            <th>Залог (руб)</th>
            <th>Статус</th>
            <th>Сумма с учетом скидки (+залог)</th>
            <th>Сумма с учетом скидки и статуса</th>";
            while ($result = mysqli_fetch_array($history, MYSQLI_NUM))
            {
                echo "<tr>
                    <td> $result[1] </td>
                    <td> $result[2] </td>
                    <td> $result[3] </td>
                    <td> $result[4] </td>
                    <td> $result[5] </td>
                    <td> $result[6]</td>";
                    if($result[6] == 'lost')
                    {
                        $lost=1;
                    }
                    if($result[6] == 'pending')
                    {
                        $lost=1;
                    }
                    if($result[6] == 'not lost')
                    {
                        $lost=0;
                    }
                    $SQLcost = "SELECT SUM(Movies.rental_cost*(DATEDIFF(Rental.return_date, Rental.rental_date)+1)*(100-Discount.discount)/100), SUM(Movies.rental_cost*(DATEDIFF(Rental.return_date, Rental.rental_date)+1)*(100-Discount.discount)/100+Movies.deposit_amount) FROM Movies INNER JOIN Rental ON Rental.movie=Movies.ID_movie INNER JOIN Clients ON Rental.client=Clients.ID_client INNER JOIN Discount ON Discount.ID_discount=Clients.discount WHERE Rental.ID_rental=$result[0]";
                    $cost = mysqli_query($link,$SQLcost);
                    while ($res = mysqli_fetch_array($cost, MYSQLI_NUM))
                    {
                        echo "<td> $res[1] </td>";
                            if($lost==1)
                            {
                                echo "<td> $res[1] </td>";
                                $s += $res[1];
                            }
                            if($lost==0)
                            {
                                echo "<td> $res[0] </td>";
                                $s += $res[0];
                            }
                    }
                echo "</tr>";
            }
        echo "</table>";


        mysqli_free_result($client);
        mysqli_free_result($history);
        mysqli_free_result($cost);
        mysqli_close($link); ?>

        <p><u><strong><div align="right">Итого: <?php echo "$s"; ?> руб</div></strong></u></p>
        <div class="footer"><a href="clients.php"> <img src="./images/back.png"> </a></div>
    </body>
</html>