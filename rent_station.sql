CREATE TABLE Studio(
    ID_studio INT AUTO_INCREMENT PRIMARY KEY,
    studio CHAR(30) NOT NULL DEFAULT ''
);

CREATE TABLE Format(
    ID_format INT AUTO_INCREMENT PRIMARY KEY,
    format CHAR(10) NOT NULL DEFAULT ''
);

CREATE TABLE Movies(
    ID_movie INT AUTO_INCREMENT PRIMARY KEY,
    title CHAR(50) NOT NULL DEFAULT '',
    genre CHAR(30) NOT NULL DEFAULT '',
    studio INT REFERENCES Studio(ID_studio) ON DELETE CASCADE ON UPDATE CASCADE,
    annotation VARCHAR(1000) NOT NULL DEFAULT '',
    languages CHAR(200) NOT NULL DEFAULT '',
    format INT REFERENCES Format(ID_format) ON DELETE CASCADE ON UPDATE CASCADE,
    poster VARCHAR(500),
    IMDB_rating FLOAT NOT NULL,
    KP_rating FLOAT NOT NULL,
    rental_cost REAL NOT NULL,
    deposit_amount REAL NOT NULL,
    amount_on_hand INT,
    quantity_in_stock INT
);

CREATE TABLE Discount(
    ID_discount INT AUTO_INCREMENT PRIMARY KEY,
    category CHAR(30) NOT NULL DEFAULT '',
    discount INT NOT NULL
);

CREATE TABLE Clients(
    ID_client INT AUTO_INCREMENT PRIMARY KEY,
    surname CHAR(30) NOT NULL DEFAULT '',
    name CHAR(30) NOT NULL DEFAULT '',
    middle_name CHAR(30),
    passport_series INT NOT NULL,
    passport_number INT NOT NULL,
    address CHAR(255) NOT NULL DEFAULT '',
    discount INT REFERENCES Discount(ID_discount) ON DELETE CASCADE ON UPDATE CASCADE,
    CHECK(passport_series<=9999 AND passport_series>999),
    CHECK(passport_number<=999999 AND passport_number>99999),
    unique(passport_series, passport_number)
);

CREATE TABLE Rental(
    ID_rental INT AUTO_INCREMENT PRIMARY KEY,
    rental_date DATE NOT NULL,
    return_date DATE NOT NULL,
    client INT REFERENCES Clients(ID_client) ON DELETE CASCADE ON UPDATE CASCADE,
    movie INT REFERENCES Movies(ID_movie) ON DELETE CASCADE ON UPDATE CASCADE,
    status CHAR(255)
);

INSERT INTO Studio VALUES(NULL, 'Walt Disney');
INSERT INTO Studio VALUES(NULL, 'Universal Pictures');
INSERT INTO Studio VALUES(NULL, 'Lionsgate');
INSERT INTO Studio VALUES(NULL, 'Pixar');
INSERT INTO Studio VALUES(NULL, 'Warner Bros.');

INSERT INTO Format VALUES(NULL, '144p');
INSERT INTO Format VALUES(NULL, '240p');
INSERT INTO Format VALUES(NULL, '360p');
INSERT INTO Format VALUES(NULL, '720p');
INSERT INTO Format VALUES(NULL, '1080p');
INSERT INTO Format VALUES(NULL, '1440p');
INSERT INTO Format VALUES(NULL, '2160p');
INSERT INTO Format VALUES(NULL, '3D');

INSERT INTO Movies VALUES(NULL, 'Zootopia', 'Family', 1, "Отважная крольчиха Джуди делает полицейскую карьеру. Ей предстоит раскрыть заговор в огромном зверином городе", 'Russian, English, Chinese', 4, 'https://lumiere-a.akamaihd.net/v1/images/movie_poster_zootopia_866a1bf2.jpeg?region=0%2C0%2C300%2C450', 8.0, 8.3, 150, 500, 1, 3);
INSERT INTO Movies VALUES(NULL, 'Mamma Mia!', 'Melodrama', 2, "Софи зовет на свадьбу троих своих возможных отцов. Мюзикл с песнями группы ABBA и звездным актерским составом", 'Russian, English', 6, 'https://avatars.mds.yandex.net/get-kinopoisk-image/1629390/0966df64-86b7-426a-8dc4-fd7af0dd3865/600x900', 6.4, 7.2, 100, 400, 2, 1);
INSERT INTO Movies VALUES(NULL, 'Wonder', 'Drama', 3, "Мальчик с необычным лицом решается пойти в обычную школу. Теплая семейная драма с Джулией Робертс", 'Russian, English', 1, 'https://www.podpisnie.ru/upload/resize_images/17542024/classic_312x460_17542024.jpeg', 8.0, 8.0, 200, 1000, 1, 1);

INSERT INTO Discount VALUES(NULL, 'Бронза', 0);
INSERT INTO Discount VALUES(NULL, 'Серебро', 3);
INSERT INTO Discount VALUES(NULL, 'Золото', 5);
INSERT INTO Discount VALUES(NULL, 'Платина', 7);
INSERT INTO Discount VALUES(NULL, 'Алмаз', 10);
INSERT INTO Discount VALUES(NULL, 'VIP', 15);

INSERT INTO Clients VALUES(NULL, 'Алексеев', 'Василий', 'Александрович', 1234, 777777, 'ул.Кулаковского 46/2, комн.701/2', 2);
INSERT INTO Clients VALUES(NULL, 'Степанов', 'Тамерлан', 'Джулустанович', 2345, 123321, 'ул.Кулаковского 46/2, комн.802/3', 2);
INSERT INTO Clients VALUES(NULL, 'Алексеева', 'Алтана', 'Александровна', 3456, 321123, 'ул.Мира 1', 3);

INSERT INTO Rental VALUES(NULL, '2021-04-12', '2021-04-14', 1, 2, 'not lost');
INSERT INTO Rental VALUES(NULL, '2021-04-13', '2021-04-15', 2, 3, 'lost');
INSERT INTO Rental VALUES(NULL, '2021-04-14', '2021-04-16', 3, 1, 'lost');
INSERT INTO Rental VALUES(NULL, '2021-04-25', '2021-05-01', 3, 2, 'not lost');