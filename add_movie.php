<html>
    <head>
        <title>Add movie</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body
            {
                background: center 100% / 100% 100% url("./images/vhs.jpg");
                font-family: 'Courier New', Courier, monospace;
                color: white;
            }
            div.footer
            {
                position: absolute;
                right: 0;
                bottom: 0;
            }
        </style>
    </head>

    <body>
        <meta charset="utf-8">
        <?php
            include('config.php');	
            $link = mysqli_connect($host, $user, $password, $database)
            or die('Error: Unable to connect: ' . mysqli_connect_error());
        ?>
        
        <form action="add_movie_form_action.php" method="POST">
            Название: <input type="text" name="title">
            <BR><BR>
            Жанр:
                <?php
                $SQLgenre = "SELECT DISTINCT genre FROM Movies ORDER BY genre";
                $genre = mysqli_query($link, $SQLgenre);
                ?>
                <input list="genre" name="genre">
                    <datalist id="genre">
                        <?php
                        while ($res = mysqli_fetch_array($genre, MYSQLI_NUM))
                        {
                            echo "<option value = '".$res[0]."'/>";
                        }
                        mysqli_free_result($genre);
                        ?>
                    </datalist>
            <BR><BR>
            Краткое описание: <input type="text" name="annotation">
            <BR><BR>
            Студия:
                <?php
                $SQLstudio = "SELECT DISTINCT studio FROM Studio ORDER BY studio";
                $studio = mysqli_query($link, $SQLstudio);
                ?>
                <input list="studio" name="studio">
                    <datalist id="studio">
                        <?php
                        while ($res = mysqli_fetch_array($studio, MYSQLI_NUM))
                        {
                            echo '<option value = "'.$res[0].'"/>';
                        }
                        mysqli_free_result($studio);
                        ?>
                    </datalist>
            <BR><BR>
            Язык: <input type="text" name="language">
            <BR><BR>
            Формат:
                <?php
                $SQLformat = "SELECT DISTINCT format FROM Format ORDER BY format";
                $format = mysqli_query($link, $SQLformat);
                ?>
                <input list="format" name="format">
                    <datalist id="format">
                        <?php
                        while ($res = mysqli_fetch_array($format, MYSQLI_NUM))
                        {
                            echo '<option value = "'.$res[0].'"/>';
                        }
                        mysqli_free_result($format);
                        ?>
                    </datalist>
            <BR><BR>
            Рейтинг по IMDB: <input type="text" name="IMDB_rating">
            <BR><BR>
            Рейтинг по КиноПоиск: <input type="text" name="KP_rating">
            <BR><BR>
            Аренда (руб./сут.): <input type="text" name="rental_cost">
            <BR><BR>
            Залог (руб.): <input type="text" name="deposit_amount">
            <BR><BR>
            Количество на руках: <input type="text" name="amount_on_hand">
            <BR><BR>
            Количество на складе: <input type="text" name="quantity_in_stock">
            <BR><BR>
            Вставьте URL картинки: <input type="text" name="poster">
            <BR><BR>
            <input type="submit" value="Добавить">
        </form>
        <?php mysqli_close($link); ?>
        <div class="footer"><a href="movies.php"> <img src="./images/back.png"> </a></div>
    </body>
</html>