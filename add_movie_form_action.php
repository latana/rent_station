<html>
    <head>
        <title>Add movie</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body
            {
                background: center 100% / 100% 100% url("./images/cassette.png");
                color: white;
                font-family: 'Courier New', Courier, monospace;
            }
            div.desc
            {
                margin: 0;
                position: absolute;
                top: 50%;
                left: 50%;
                margin-right: -50%;
                transform: translate(-50%, -50%);
                width: 500px;
                color: black;
                font-family: 'Courier New', Courier, monospace;
                font-size: 50px;
            }
            div.footer
            {
                position: absolute;
                right: 0;
                bottom: 0;
            }
        </style>
    </head>

    <body>
<?php
    include('config.php');	
    $link = mysqli_connect($host, $user, $password, $database)
        or die('Error: Unable to connect: ' . mysqli_connect_error());

    $title = mysqli_real_escape_string($link, $_POST['title']);
    $genre = mysqli_real_escape_string($link, $_POST['genre']);
    $annotation = mysqli_real_escape_string($link, $_POST['annotation']);
    $language = mysqli_real_escape_string($link, $_POST['language']);
    $IMDB_rating = mysqli_real_escape_string($link, $_POST['IMDB_rating']);
    $KP_rating = mysqli_real_escape_string($link, $_POST['KP_rating']);
    $rental_cost = mysqli_real_escape_string($link, $_POST['rental_cost']);
    $deposit_amount = mysqli_real_escape_string($link, $_POST['deposit_amount']);
    $amount_on_hand = mysqli_real_escape_string($link, $_POST['amount_on_hand']);
    $quantity_in_stock = mysqli_real_escape_string($link, $_POST['quantity_in_stock']);
    $poster = mysqli_real_escape_string($link, $_POST['poster']);

    $k=0;

    $SQLquery = "INSERT INTO Movies (ID_movie, title, genre, annotation, languages, poster, IMDB_rating, KP_rating, rental_cost, deposit_amount, amount_on_hand, quantity_in_stock)
    VALUES (NULL, '$title', '$genre', '$annotation', '$language', '$poster', '$IMDB_rating', '$KP_rating', '$rental_cost', '$deposit_amount', '$amount_on_hand', '$quantity_in_stock')";
    if(mysqli_query($link, $SQLquery))
    {
        $k++;
    }

    $studio = mysqli_real_escape_string($link, $_POST['studio']);
        $SQLstudio = "SELECT COUNT(studio) FROM Studio WHERE studio='$studio'";
        $SQLstudioresult = mysqli_query($link, $SQLstudio);
        while ($res = mysqli_fetch_array($SQLstudioresult, MYSQLI_NUM))
        {
            if ($res[0] == 0)
            {
                $SQLnewstudio = "INSERT INTO Studio VALUES (NULL, '".$studio."')";
                $newstudio = mysqli_query($link, $SQLnewstudio);
            }
            $SQLidstudio = "SELECT ID_studio FROM Studio WHERE studio = '".$studio."'";
            $idstudio = mysqli_query($link, $SQLidstudio);
            while ($result = mysqli_fetch_array($idstudio, MYSQLI_NUM))
            {
                $SQLqueryS = "UPDATE Movies SET studio = '".$result[0]."' WHERE title = '".$title."'";
                if(mysqli_query($link, $SQLqueryS))
                {
                    $k++;
                }
                //$SQLresultS = mysqli_query($link, $SQLqueryS);
            }
        }
    $format = mysqli_real_escape_string($link, $_POST['format']);
        $SQLformat = "SELECT COUNT(format) FROM Format WHERE format = '".$format."'";
        $SQLformatresult = mysqli_query($link, $SQLformat);
        while ($res = mysqli_fetch_array($SQLformatresult, MYSQLI_NUM))
        {
            if($res[0] == 0)
            {
                $SQLnewformat = "INSERT INTO Format VALUES (NULL, '".$format."')";
                $newformat = mysqli_query($link, $SQLnewformat);
            }
            $SQLidformat = "SELECT ID_format FROM Format WHERE format = '".$format."'";
            $idformat = mysqli_query($link, $SQLidformat);
            while ($result = mysqli_fetch_array($idformat, MYSQLI_NUM))
            {
                $SQLqueryF = "UPDATE Movies SET format = '".$result[0]."' WHERE title = '".$title."'";
                if(mysqli_query($link, $SQLqueryF))
                {
                    $k++;
                }
            }
        }
    mysqli_close($link);
?>
    <!DOCTYPE html>
<html>
    <head>
        <title>Add movie</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style type = "text/css">
            body
            {
                background: center 100% / 100% 100% url("./images/cassette.png");
            }
            div.info
            {
                margin: 0;
                position: absolute;
                top: 54%;
                left: 50%;
                margin-right: -50%;
                transform: translate(-50%, -50%);
                width: 500px;
                height: 330px;
                color: black;
                font-family: 'Courier New', Courier, monospace;
            }
            div.footer
            {
                position: absolute;
                right: 0;
                bottom: 0;
            }
        </style>
    </head>

    <body>
        <?php
        if($k == 3)
        {
            echo "<div class='desc'> Фильм добавлен успешно! </div>";
        }
        else
        {
            echo "<div class='desc'> Произошла ошибка( </div>";
        }
        ?>
        <div class="footer"><a href="movies.php"> <img src="./images/back.png"> </a></div>
    </body>
</html>