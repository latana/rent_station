<html>
    <head>
        <title>Remove movie</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body
            {
                background: center 100% / 100% 100% url("./images/cassette.png");
                color: white;
                font-family: 'Courier New', Courier, monospace;
            }
            div.desc
            {
                margin: 0;
                position: absolute;
                top: 50%;
                left: 50%;
                margin-right: -50%;
                transform: translate(-50%, -50%);
                width: 500px;
                color: black;
                font-family: 'Courier New', Courier, monospace;
                font-size: 50px;
            }
            div.footer
            {
                position: absolute;
                right: 0;
                bottom: 0;
            }
        </style>
    </head>

    <body>
        <?php
        include('config.php');	
        $link = mysqli_connect($host, $user, $password, $database)
            or die('Error: Unable to connect: ' . mysqli_connect_error());

        $id = $_GET['id'];

        $SQLquery = "DELETE FROM Movies WHERE ID_movie=$id";
        if(mysqli_query($link, $SQLquery))
        {
            echo "<div class='desc'> Фильм успешно удален </div>";
        }
        else
        {
            echo "<div class='desc'> Произошла ошибка </div>";
        }
        mysqli_close($link);
        ?>
        <div class="footer"><a href="movies.php"> <img src="./images/back.png"> </a></div>
    </body>
</html>