<html>
    <head>
        <title>Add movie</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body
            {
                background: center 100% / 100% 100% url("./images/vhs.jpg");
                font-family: 'Courier New', Courier, monospace;
                color: white;
            }
            div.footer
            {
                position: absolute;
                right: 0;
                bottom: 0;
            }
        </style>
    </head>

    <body>
        <meta charset="utf-8">
        <?php
            include('config.php');	
            $link = mysqli_connect($host, $user, $password, $database)
            or die('Error: Unable to connect: ' . mysqli_connect_error());
            
            $SQLdiscount = "SELECT ID_discount, category, discount FROM Discount";
            $discount = mysqli_query($link, $SQLdiscount);
        ?>
        
        <form action="add_client_form_action.php" method="POST">
            Фамилия: <input type="text" name="surname">
            <BR><BR>
            Имя: <input type="text" name="name">
            <BR><BR>
            Отчество: <input type="text" name="middle_name">
            <BR><BR>
            Серия паспорта: <input type="text" name="passport_series">
            <BR><BR>
            Номер паспорта: <input type="text" name="passport_number">
            <BR><BR>
            Адрес: <input type="text" name="address">
            <BR><BR>
            Статус:
            <select name="discount">
                <option> ---- </option>
                <?php
                while($object = mysqli_fetch_array($discount, MYSQLI_NUM))
                {
                    echo "<option value = '".$object[0]."'>" .$object[1]." - " .$object[2]. "% </option>";
                }
                mysqli_free_result($discount);
                ?>
            </select>
            <BR><BR>
            <input type="submit" value="Добавить">
        </form>
        <?php mysqli_close($link); ?>
        <div class="footer"><a href="clients.php"> <img src="./images/back.png"> </a></div>
    </body>
</html>