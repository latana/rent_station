<html>
    <head>
        <title>Clients</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body
            {
                background: center 100% / 100% 100% url("./images/vhs.jpg");
            }
            div.client
            {
                border: 5px double white;
            }
            div.client:hover
            {
                border: 5px double red;
            }
            div.desc
            {
                text-align: center;
                color: white;
                font-family: 'Courier New', Courier, monospace;
                font-size: 50px;
            }
            a
            {
                text-decoration: none;
            }
            a:visited
            {
                color: white;
            }
            div.footer
            {
                position: absolute;
                right: 0;
                bottom: 0;
            }
        </style>
    </head>

    <body>
        <div class='desc'> Кого хотите удалить? </div>
        <?php
        include('config.php');	
        $link = mysqli_connect($host, $user, $password, $database)
            or die('Error: Unable to connect: ' . mysqli_connect_error());
            
        $SQLquery = "SELECT ID_client, surname, name, middle_name FROM Clients ORDER BY surname";
        $SQLresult = mysqli_query($link,$SQLquery);
        
        while ($result = mysqli_fetch_array($SQLresult, MYSQLI_NUM))
        {
            echo
            "<div class='client'>
                <a target='_self' href='remove_client_form_action.php?id=".$result[0]."'>
                    <div class='desc'>$result[1] $result[2] $result[3]</div>
                </a>
            </div>
            <br>";
        }
        mysqli_free_result($SQLresult);
        mysqli_close($link);
        ?>
        <div class="footer"><a href="clients.php"> <img src="./images/back.png"> </a></div>
    </body>
</html>