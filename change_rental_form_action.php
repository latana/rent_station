<html>
    <head>
        <title>Change the rental</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body
            {
                background: center 100% / 100% 100% url("./images/vhs.jpg");
                color: white;
                font-family: 'Courier New', Courier, monospace;
            }
            div.desc
            {
                text-align: center;
                color: white;
                font-family: 'Courier New', Courier, monospace;
                font-size: 50px;
            }
            div.footer
            {
                position: absolute;
                right: 0;
                bottom: 0;
            }
        </style>
    </head>

    <body>
        <?php
        include('config.php');	
        $link = mysqli_connect($host, $user, $password, $database)
            or die('Error: Unable to connect: ' . mysqli_connect_error());

        $id = $_GET['id'];
        $fio = mysqli_real_escape_string($link, $_POST['fio']);
        $movie = mysqli_real_escape_string($link, $_POST['movie']);
        $rental_date = mysqli_real_escape_string($link, $_POST['rental_date']);
        $return_date = mysqli_real_escape_string($link, $_POST['return_date']);
        $status = mysqli_real_escape_string($link, $_POST['status']);

        $SQLquery = "UPDATE Rental SET client='$fio', movie='$movie', rental_date='$rental_date', return_date='$return_date', status='$status' WHERE ID_rental=$id";
        if(mysqli_query($link, $SQLquery))
        {
            echo "<div class='desc'> Данные успешно изменены </div>";
        }
        else
        {
            echo "<div class='desc'> Произошла ошибка </div>";
        }
        mysqli_close($link);
        ?>
        <div class="footer"><a href="current_state.php"> <img src="./images/back.png"> </a></div>
    </body>
</html>