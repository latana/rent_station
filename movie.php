<html>
    <head>
        <title>About movie</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style type = "text/css">
            body
            {
                background: center 100% / 100% 100% url("./images/cassette.png");
            }
            div.info
            {
                margin: 0;
                position: absolute;
                top: 54%;
                left: 50%;
                margin-right: -50%;
                transform: translate(-50%, -50%);
                width: 500px;
                height: 330px;
                color: black;
                font-family: 'Courier New', Courier, monospace;
            }
            div.footer
            {
                position: absolute;
                right: 0;
                bottom: 0;
            }
        </style>
    </head>

    <body>
    <?php
        $id = $_GET['id'];

        include('config.php');	
        $link = mysqli_connect($host, $user, $password, $database)
            or die('Error: Unable to connect: ' . mysqli_connect_error());

        $SQLquery = "SELECT Movies.title, Movies.genre, Movies.annotation, Studio.studio, Movies.languages, Format.format, Movies.IMDB_rating, Movies.KP_rating, Movies.rental_cost, Movies.deposit_amount, Movies.amount_on_hand, Movies.quantity_in_stock FROM Movies INNER JOIN Studio ON Studio.ID_studio=Movies.studio INNER JOIN Format ON Format.ID_format=Movies.format WHERE Movies.ID_movie=$id";
        $SQLresult = mysqli_query($link,$SQLquery);

        while ($result = mysqli_fetch_array($SQLresult, MYSQLI_NUM))
        {
            echo "<div class='info'>";
                echo "<strong>Название:</strong> $result[0]";
                echo "<br>";
                echo "<strong>Жанр:</strong> $result[1]"; echo "<br>";
                echo "<strong>Краткое описание:</strong> $result[2]"; echo "<br>";
                echo "<strong>Студия:</strong> $result[3]"; echo "<br>";
                echo "<strong>Язык:</strong> $result[4]"; echo "<br>";
                echo "<strong>Формат:</strong> $result[5]"; echo "<br>";
                echo "<strong>Рейтинг по IMDB:</strong> $result[6]"; echo "<br>";
                echo "<strong>Рейтинг по КиноПоиск:</strong> $result[7]"; echo "<br>";
                echo "<strong>Аренда:</strong> $result[8]"; echo " руб/сут"; echo "<br>";
                echo "<strong>Залог:</strong> $result[9]"; echo " руб"; echo "<br>";
                echo "<strong>Количество на руках:</strong> $result[10]"; echo "<br>";
                echo "<strong>Количество на складе:</strong> $result[11]";
            echo "</div>";
        }

        mysqli_free_result($SQLresult);
        mysqli_close($link);
    ?>
        <div class="footer"><a href="movies.php"> <img src="./images/back.png"> </a></div>
    </body>
</html>