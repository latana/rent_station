<html>
    <head>
        <title>Cash transactions</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body
            {
                background: center 100% / 100% 100% url("./images/vhs.jpg");
                font-family: 'Courier New', Courier, monospace;
                color: white;
            }
            table
            {
                width: 100%;
                font-family: 'Courier New', Courier, monospace;
                color: white;
                border-color: white;
            }
            div.footer
            {
                position: absolute;
                right: 0;
                bottom: 0;
            }
        </style>
    </head>

    <body>
        <?php
        include('config.php');	
        $link = mysqli_connect($host, $user, $password, $database)
            or die('Error: Unable to connect: ' . mysqli_connect_error());
        
        $SQLrental = "SELECT Rental.ID_rental, Clients.surname, Clients.name, Clients.middle_name, Rental.status, Movies.deposit_amount FROM Clients INNER JOIN Rental ON Clients.ID_client=Rental.client INNER JOIN Movies ON Movies.ID_movie=Rental.movie INNER JOIN Discount ON Discount.ID_discount=Clients.discount GROUP BY Rental.ID_rental ORDER BY Rental.rental_date";
        $rental = mysqli_query($link,$SQLrental);

        $s=0;
        ?>
        <table border="1">
            <th>ФИО</th>
            <th>Оплатил</th>
            <th>Возвращение залога</th>
            <th>Начислено</th>
            <?php
            while ($result = mysqli_fetch_array($rental, MYSQLI_NUM))
            {
                echo "<tr>
                    <td> $result[1] $result[2] $result[3] </td>";
                    if($result[4] == 'lost')
                    {
                        $lost=1;
                    }
                    if($result[4] == 'pending')
                    {
                        $lost=1;
                    }
                    if($result[4] == 'not lost')
                    {
                        $lost=0;
                    }
                    $SQLcost = "SELECT SUM(Movies.rental_cost*(DATEDIFF(Rental.return_date, Rental.rental_date)+1)*(100-Discount.discount)/100), SUM(Movies.rental_cost*(DATEDIFF(Rental.return_date, Rental.rental_date)+1)*(100-Discount.discount)/100+Movies.deposit_amount) FROM Clients INNER JOIN Rental ON Clients.ID_client=Rental.client INNER JOIN Movies ON Movies.ID_movie=Rental.movie INNER JOIN Discount ON Discount.ID_discount=Clients.discount WHERE Rental.ID_rental=$result[0] ORDER BY Rental.rental_date";
                    $cost = mysqli_query($link,$SQLcost);
                    while ($res = mysqli_fetch_array($cost, MYSQLI_NUM))
                    {
                        echo "<td> $res[1] </td>";
                        if($lost==1)
                        {
                            echo "<td> no ($result[5] руб) </td>
                            <td> + $res[1] руб </td>";
                            $s += $res[1];
                        }
                        if($lost==0)
                        {
                            echo "<td> yes ($result[5] руб) </td>
                            <td> + $res[0] руб </td>";
                            $s += $res[0];
                        }
                    }
                echo "</tr>";
            }
            ?>
        </table>
        
        <p><u><strong><div align="right">Всего начислено: <?php echo "$s"; ?> руб</div></strong></u></p>
        <div class="footer"><a href="index.html"> <img src="./images/back.png"> </a></div>
    </body>
</html>